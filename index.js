let getCube = (number) => console.log(`The cube of ${number} is ${number ** 3}.`);
getCube(2);

let address = ["258", "Washington Ave NW", "California", "90011"];
let [houseNum, streetName, state, zipCode] = address;
console.log(`I live at ${houseNum} ${streetName}, ${state} ${zipCode}.`);

let animal = {
	name: "Lolong",
	specie: "Saltwater Crocodile",
	weight: "1075 kg",
	length: "20 ft 3in"
};
let {name, specie, weight, length} = animal;
console.log(`${name} was a ${specie.toLowerCase()}. He weighed at ${weight} with a measurement of ${length}.`);

let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(number));

class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed
	};
};
let myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);